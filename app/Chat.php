<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';
    protected $fillable = [
        'message'
    ];
    public $timestamps = false;

    public static function show() {
        $message = Chat::orderBy('id', 'DESC')->take(5)->get();
        return $message;
    }

    public static function addMessage($message) {
        $chat = new Chat;
        $chat->user_id = $message['user_id'];
        $chat->message = $message['message'];
        $chat->time = date("Y-m-d H:i:s");
        $chat->save();
    }
}

