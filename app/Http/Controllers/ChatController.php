<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Chat;
use Illuminate\Support\Facades\Session;
use Validator, Redirect, Request, Compiled;

class ChatController extends Controller
{
    public function showChatWindow ()
    {
        $messages = Chat::show();
        return view('/chat', ['messages' => $messages]);
    }
    public function SendMessage ()
    {
        $message = Request::input();
        $session = Session::get('user_id');
        var_dump($session);
        if (!Session::has('user_id')) {
            $userId = rand(1, 100);
            Session::put('user_id', $userId);
            Session::save();
        };
        $message['user_id'] = Session::get('user_id');
        if ($message['message'] != '') {
            Chat::addMessage($message);
        }
        $messages = Chat::show();
        return view('/chat', ['messages' => $messages]);
    }
}
