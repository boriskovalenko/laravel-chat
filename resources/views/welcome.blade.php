@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('style')
@endsection

@section('content')
    <div class="container">
        <div class="content">
            <div class="title"><a href="/chat">Chat</a> on <a href="//laravel.com">Laravel 5</a></div>
        </div>
    </div>
@endsection
