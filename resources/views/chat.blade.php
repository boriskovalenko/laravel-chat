@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('style')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                {!! Form::open(array(
                    'url' => action('ChatController@SendMessage'),
                    'method' => 'post',
                    'role' => 'form')) !!}
                    <div class="form-group">
                        {!! Form::text('message', null, array('class' => 'form-control')) !!}
                        {!! Form::submit('Отправить') !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3" id="messageWindow">
                @foreach($messages as $message)
                    <p>({{ $message['time']}}) "{{ $message['user_id']}}" --- {{$message['message'] }}</p>
                @endforeach
            </div>
        </div>
    </div>
@endsection
